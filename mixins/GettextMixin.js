let gettext = window.gettext || function (s) { return s }

export default {
  methods: {
    gettext (s) {
      return gettext(s)
    }
  }
}
