# pai-vue-gettext

> Vuejs gettext utils

This is alpha software and is under heavy development.

## Deps
https://www.gnu.org/software/gettext/manual/html_node/xgettext-Invocation.html#xgettext-Invocation
https://github.com/mikeedwards/po2json


## Howto

```bash
# Create messages
$ ./makemessages.sh

# Fill up generated .mo files (locale/<lang>/messages.mo)

# Compile messages

$ ./compilemessages.sh

```
