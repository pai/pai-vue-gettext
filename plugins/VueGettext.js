/*
  VueJs Gettext Plugin
*/

/*global window*/

const getNavigatorLanguage = () => {
  if (navigator.languages && navigator.languages.length) {
    return navigator.languages[0];
  } else {
    return navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en';
  }
}

if (!window.django) {
  var locale = getNavigatorLanguage().split('-')[0]

  window.locale = window.locale || locale
  window.catalog = window.catalog || {en:{}}
  window.gettext = window.gettext || function (msgid) {
    var catalog = window.catalog[window.locale] || window.catalog['en']
    var value = catalog[msgid]
      if (typeof(value) === 'undefined') {
        return msgid
      } else {
        return (typeof(value) === 'string') ? (value.length ? value : msgid) : (value[0] && value[0].length ? value[0] : msgid)
      }
  }
}

const gettext = window.gettext || function (s) { return s }

const _options = {
  locale: window.locale,
  callback: function () {}
}

const VueGettext = {
  install (Vue, opts) {
    const options = { ..._options, ...opts }

    const root = new Vue({
      data: { locale: options.locale },
      methods: {
        gettext (s) {
          return gettext(s)
        },
        setLocale (code) {
          options.callback(code)

          window.locale = code
          this.locale = code

          this.$emit("localeChanged", code)
        }
      }
    })

    Vue.mixin({
      data () {
        return {
          locale: root.locale
        }
      },
      mounted () {
        root.$on('localeChanged', (code) => {
          this.locale = code
        })

        root.setLocale(root.locale)
      },
      methods: {
        gettext (s) {
          return root.gettext(s)
        },
        setLocale (l) {
          root.setLocale(l)
        }
      },
      watch: {
        locale: function (n) {
          if (n) {
            this.$emit('localeChanged', n)
          }
        }
      }
    })

  }
}

export default VueGettext
