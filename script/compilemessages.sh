#!/bin/bash

set -e

# it
./node_modules/.bin/po2json -p -f mf locale/it/messages.po public/locale/it/messages.js
echo -e "window.catalog=window.catalog || {en:{}};\nwindow.catalog.it=$(cat public/locale/it/messages.js);" > public/locale/it/messages.js
