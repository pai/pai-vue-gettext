#!/bin/bash

set -e

./create_gettext_file.py

mkdir -p locale/it/
xgettext --from-code=UTF-8 --language=JavaScript -o locale/messages.pot gettext_strings.js

sed -i -- 's/charset=CHARSET/charset=UTF-8/g' locale/messages.pot

cp locale/it/messages.po locale/it/messages.po.backup
msginit --no-translator --input locale/messages.pot --locale it --output locale/it/messages.po
